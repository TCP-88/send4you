import os
import logging
import hashlib as hash
import paramiko


logging.basicConfig(filename='/data/dev/ftp/ftp.log', level=logging.DEBUG,
    format='%(asctime)s:%(levelname)s:%(message)s')

# archive = "/data/dev/ftp/upload/archive/"
# localdir = "/data/dev/ftp/upload/"
# fileext = ".txt"
# remotedir= "/home/user1/"
# dishost = "10.15.1.83"
# username = "user1"
# password = "password"
# port = "22"

# def job(id):
#     jobid = Task.query.filter_by(jobid=f'{id}').first()
#     archive = jobid.archive
#     localdir = jobid.localdir
#     fileext = jobid.fileext
#     remotedir = jobid.remotedir
#     dishost = jobid.dishost
#     username = jobid.username
#     password = jobid.password
#     port =jobid.port


# id = "job1"
# job(id)


# Check local file is exist
def checkfile(localdir, fileext):
    logging.debug(f"checking if file with extention is exist in {localdir}")
    file = [f for f in os.listdir(localdir) if f.endswith(fileext)]
    return file

#create file hash    
#def hashf(f):
#    BLOCKSIZE = 65536
#    sha = hash.sha256()
#    file = open(f, 'rb')
#    buffer = file.read(BLOCKSIZE)
#    while len(buffer) > 0:
#        sha.update(buffer)
#        buffer = file.read(BLOCKSIZE)      
#    return sha.hexdigest()
#has = hashf("/data/dev/ftp/upload/list.txt")

#send file
def send(dir, file, host, id, pw, port, rdir):
    #paramiko.util.log_to_file('ssh.log')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=host, username=id, password=pw, port=port)
    sftp_client=ssh.open_sftp()
    try:
        sftp_client.put(dir+file, rdir+file, confirm=True)
    except IOError:
        return "error"
    except OSError:
        return "error"
    finally:
        sftp_client.close()
        ssh.close()

#receive
def receive(dir, file, host, id, pw, port, rdir):
    #paramiko.util.log_to_file('ssh.log')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=host, username=id, password=pw, port=port)
    sftp_client=ssh.open_sftp()
    try:
        sftp_client.put(dir+file, rdir+file, confirm=True)
    except IOError:
        return "error"
    except OSError:
        return "error"
    finally:
        sftp_client.close()
        ssh.close()

#execute send function 
# def transfernow():
#     for f in checkfile():
#         result = send(localdir, f, dishost, username, password, port, remotedir)
#         if result != "error":
#             print("no error move file now")
#             os.system(f"mv {localdir}{f} {archive}{f}")
#             f = [{f'status:0','host:{dishost}'}]
#         else:
#             print("error")


