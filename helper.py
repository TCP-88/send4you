# import os
# import requests
# import urllib.parse
# import pwd, grp



# def listuser(user, group, home, ):
#     user = pwd.getpwnam(id).pw_name
#     group = grp.getgrnam(id).gr_name
#     home = pwd.getpwnam(id).pw_dir
#     shell = pwd.getpwnam(id).pw_shell
#     pw = pwd.getpwnam(id).pw_passwd
import requests
from flask import redirect, session
from functools import wraps

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("username") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function