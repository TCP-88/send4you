import pwd, grp
#from apscheduler.job import Job
#from requests.api import get
from simplepam import authenticate
from flask import Flask, render_template, redirect, url_for, request, flash, send_file, jsonify, session
import os
from pathlib import Path
import crypt
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from flask_apscheduler import APScheduler, scheduler
#from json2html import *
from tinydb import TinyDB, Query
from tinydb.operations import delete
#import requests ,json
from helper import login_required
#from flask_apscheduler.auth import HTTPBasicAuth

app = Flask(__name__)
# app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.secret_key = '962e5dbae6eb336fd730fe25122d601c'
scheduler = APScheduler()

#scheduler-config============================================
class Config:
    SCHEDULER_JOBSTORES = {
        "default": SQLAlchemyJobStore(url="sqlite:///./sch.db") 
    }
    
    # SCHEDULER_API_ENABLED = True
    # SCHEDULER_AUTH = HTTPBasicAuth()
    #SCHEDULER_ALLOWED_HOSTS = ["localhost"]
    
app.config.from_object(Config())
scheduler.init_app(app)
scheduler.start()
#scheduler-config============================================

#json Task DB  
class DB:
    def __init__(self,db_path):
        self.db = TinyDB(db_path)

    def add(self, data):
        # Only add it if you can't find it
        Track = Query()
        if not self.db.get(Track.jobname == data['jobname']):
            return self.db.insert(data)

    def searchById(self, data):
        Track = Query()
        # i = self.db.get(Track.jobname == data['jobname'])
        # id =i.doc_id
        return self.db.get(Track.jobname == data)
    
    def change(self, data):
        Track = Query()
        i = self.db.get(Track.jobname == data['jobname'])
        id =i.doc_id
        return self.db.update(data, doc_ids=[id])
    
    def delall(self, data):
        Track = Query()
        i = self.db.get(Track.jobname == data['jobname'])
        id =i.doc_id
        return self.db.remove(doc_ids=[id])
    
    def delkey(self, data, id):
        i = [id]
        print(self.db.update(delete(data), doc_ids=i))

    def all(self):
        return self.db.all()

db = DB('./task.json')


@app.route("/")
def index():
    if 'username' in session:
        #flash ('Already logged in as %s') % escape(session['username'])
        return redirect(url_for('home'))
    flash ('You are not logged in') 
    return redirect(url_for('login'))

@app.route("/login", methods=["GET","POST"])
def login():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']
        if authenticate(str(username), str(password)):
            session['username'] = request.form['username']
            return redirect(url_for('home'))
        else:
            flash ('Your Password is Invaild')
            #return 'Invaild username or password'
    return render_template('login.html', title="login")

@app.route('/home', methods=["GET","POST"])
@login_required
def home():
    return render_template('home.html')

@app.route('/logout', methods=["GET","POST"])
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))

@app.route("/user", methods=["GET"])
@login_required
def user():
    if request.method == 'GET':
        #def list():
            u = pwd.getpwall()
            g = grp.getgrall()
            return render_template('user.html', title="User", user=u, group=g)

@app.route("/listu/<id>", methods=["GET"])
@login_required
def listu(id):
    if request.method == "GET":
        user = pwd.getpwnam(id).pw_name
        group = grp.getgrnam(id).gr_name
        home = pwd.getpwnam(id).pw_dir
        shell = pwd.getpwnam(id).pw_shell
        pw = pwd.getpwnam(id).pw_passwd
        dec = pwd.getpwnam(id).pw_gecos
        #pkey = os.system(f"cat /home/{id}/.ssh/id_rsa.pub")
        if id == user:
            key = os.path.isdir(f'/home/{id}/.ssh/')
            def getkey():
                file = open(f"/home/{id}/.ssh/id_rsa.pub", "r")
                pkey = file.read()
                return pkey
            if key == True:
                return render_template('euser.html', title="Edit User", user=id, group=group, home=home, shell=shell, pw=pw, pkey=getkey(), dec=dec)
            else:
                return render_template('euser.html', title="Edit User", user=id, group=group, home=home, shell=shell, pw=pw, pkey=False, dec=dec)

@app.route("/listg/<id>", methods=["GET"])
@login_required
def listg(id):
    if request.method == "GET":
        user = pwd.getpwnam(id).pw_name
        group = grp.getgrnam(id).gr_name
        home = pwd.getpwnam(id).pw_dir
        shell = pwd.getpwnam(id).pw_shell
        pw = pwd.getpwnam(id).pw_passwd
        pkey = os.system(f"cat /home/{id}/.ssh/id_rsa.pub")
        print(pkey)
        if id == user:
            key = os.path.isdir(f'/home/{id}/.ssh/id_rsa.pub')
            if key == True:
                print(pkey)
                print(key)
                return render_template('egroup.html', title="Edit User", user=id, group=group, home=home, shell=shell, pw=pw, pkey=pkey)
            else:
                return render_template('egroup.html', title="Edit User", user=id, group=group, home=home, shell=shell, pw=pw)

@app.route("/euser", methods=["POST"])
@login_required
def euser():
    if request.method == "POST":
        user = request.form.get('user-id')
        login = request.form.get('login')
        dis = request.form.get('dec-id')
        #print(user, group, home, shell)
        if login == "login":
            result = os.system(f"usermod -s /bin/bash -c '{dis}' {user}")
            if result != 0:
                flash(f'Edit User Failed with error{result}')
                return redirect(url_for('listu', id=user))
            else:
                flash(f'Save successfull {result}')
                return redirect(url_for('listu', id=user))
        else:
            result = os.system(f"usermod -s /sbin/nologin -c '{dis}' {user}")
            if result != 0:
                flash(f'Edit User Failed with error{result}')
                return redirect(url_for('listu', id=user))
            else:
                flash(f'Save successfull {result}')
                return redirect(url_for('listu', id=user))

@app.route("/cpassword", methods=["POST"])
@login_required
def cpassword():
    if request.method == "POST":
        user = request.form.get('user')
        pw = request.form.get('password')
        encPass = crypt.crypt(pw,"22") 
        #print(user, pw)
        result = os.system(f'echo "{user}:{encPass}"|chpasswd -e')
        if result != 0:
            flash(f'Password Change Failed{result}')
            return redirect(url_for('listu', id=user))
        else:
            flash(f'Password Reset successfull {result}')
            return redirect(url_for('listu', id=user))

@app.route("/egroup", methods=["POST"])
@login_required
def egroup():
    if request.method == "POST":
        user = request.form.get('user-id')
        group = request.form.get('group-id')
        home = request.form.get('home-id')
        shell = request.form.get('shell-id')
        pw = request.form.get('pw-id')
        print(user, group, home, shell, pw)
        return "save edit group change:\n"

@app.route("/adduser", methods=["GET","POST"])
@login_required
def adduser():
    if request.method == "GET":
        return render_template('adduser.html', title="Adduser")
    if request.method == "POST":
        user = request.form.get('user-id')
        pw = request.form.get('password')
        login = request.form.get('login')
        dis = request.form.get('dec-id')
        if login == "login":
                encPass = crypt.crypt(pw,"22") 
                result = os.system("useradd -p "+encPass+ " -s "+ "/bin/bash "+ "-d "+ "/home/" + user+ " -m "+ " -c \""+ dis+"\" " + user)
                if result != 0:
                    flash(f'Create User failed with error{result}', 'error')
                    return render_template('adduser.html', title="Adduser")
                else:
                    flash(f'Create User successfull {result}')
                    return redirect(url_for('index'))
        else:
                encPass = crypt.crypt(pw,"22") 
                result = os.system("useradd -p "+encPass+ " -s "+ "/sbin/nologin "+ "-d "+ "/home/" + user+ " -m "+ " -c \""+ dis+"\" " + user)
                if result != 0:
                    flash(f'Create User failed with error{result}', 'error')
                    return render_template('adduser.html', title="Adduser")
                else:
                    flash(f'Create User successfull {result}')
                    return redirect(url_for('index'))


@app.route("/addgroup", methods=["GET","POST"])
@login_required
def addgroup():
    if request.method == "GET":
        return render_template('addgroup.html', title="Adduser")
    if request.method == "POST":
        user = request.form.get('user-id')
        pw = request.form.get('pw-id')
        login = request.form.get('login')
        print(login)
        return "from euser post"

@app.route("/deluser", methods=["GET", "POST"])
@login_required
def deluser():
    if request.method == "POST":
        user = request.form.get('pkey')
        deluser = request.form.get('deluser')
        if deluser == pwd.getpwnam(deluser).pw_name:
            result = os.system(f"userdel -r {deluser}")
            if result == 0:
                flash(f"Delete success {result}")
                return redirect(url_for('index'))
            else:
                flash(f"Delete failed with errop {result}")
                return redirect(url_for('index'))

@app.route("/genkey", methods=["POST"])
@login_required
def genkey():
    if request.method == "POST":
        user = request.form.get('genkey')
        def genkey(user):
            Path(f'/home/{user}/.ssh').mkdir(parents=True, exist_ok=True)
            key = rsa.generate_private_key(
                backend=crypto_default_backend(),
                public_exponent=65537,
                key_size=2048
            )
            private_key = key.private_bytes(
                crypto_serialization.Encoding.PEM,
                crypto_serialization.PrivateFormat.PKCS8,
                crypto_serialization.NoEncryption())
            public_key = key.public_key().public_bytes(
                crypto_serialization.Encoding.OpenSSH,
                crypto_serialization.PublicFormat.OpenSSH
            )
            private = open(f'/home/{user}/.ssh/id_rsa', 'wb')
            private.write(private_key)
            private.close
            public = open(f'/home/{user}/.ssh/id_rsa.pub', 'wb')
            public.write(public_key)
            public.close
        result = genkey(user)
        os.chmod(f'/home/{user}/.ssh/id_rsa.pub', 0o600)
        os.chmod(f'/home/{user}/.ssh/id_rsa', 0o400)
        uid = pwd.getpwnam(user).pw_uid
        gid = grp.getgrnam(user).gr_gid
        os.chown(f'/home/{user}/.ssh/id_rsa.pub', uid, gid)
        os.chown(f'/home/{user}/.ssh/id_rsa', uid, gid)
        os.chown(f'/home/{user}/.ssh', uid, gid)
        if result == 0:
            flash(f"Create Key pair successful with {result}")
            return redirect(url_for('listu', id=user))
        else:
            flash(f"Create Keypair failed wth error {result}")
            return redirect(url_for('listu', id=user))

@app.route("/download-key", methods=["GET", "POST"])
@login_required
def download_key():
    if request.method == "POST":
        user = request.form.get('pkey')
        #print(user)
        dir = f"/home/{user}/.ssh/id_rsa.pub"
        #try:
        return send_file(dir, as_attachment=True)
        #except FileNotFoundError:

# #show all task
@app.route("/show-task", methods=["GET"])
@login_required
def show_task():
    #all_tasks = Task.query.all()
    #all=tasks_schema.dump(all_tasks)
    all = db.all()
    return render_template('show-task.html',all=all)

# #edit task
@app.route("/edit-task/<id>", methods=["GET","POST"])
@login_required
def edit_task(id):
    if request.method == "GET":
        #all_tasks = Task.query.filter_by(id=id)
        #all=tasks_schema.dump(all_tasks)
        #all = db.all()
        i = db.searchById(f'{id}')
        print(id)
        return render_template('etask.html', all=i)
    if request.method == "POST":
        content = request.form
        print(content)
        i = db.change(content)
        return f"testing{i}"
    
@app.route("/addtask", methods=["GET","POST"])
@login_required
def addtask():
    if request.method == "GET":
        return render_template('addtask.html')
    if request.method == "POST":
        add = request.form
        try:
            db.add(add)
            return redirect(url_for('show_task'))
        except OSError:
            return OSError
        finally:
            return redirect(url_for('show_task'))
        
@app.route("/deltask", methods=["POST"])
@login_required
def deltask():
    d = request.form.get('jobname')
    id = db.searchById(f'{d}')
    result = db.delall(id)
    if result > [0]:
        return redirect(url_for('show_task'))
    else:
        return "del failed"

@app.route("/show-scheduler", methods=["GET"])
def showsch():
    joblist = scheduler.get_jobs()      
    return render_template('sch.html', joblist=joblist)

@app.route("/esch/<id>", methods=["GET", "POST"])
def esch(id):
    b = scheduler.get_job(id)
    v = []
    v.append(b)
    cron = []
    for job in v:
        jobdict = {}
        id = job.id
        for f in job.trigger.fields:
            curval = str(f)
            jobdict[f.name] = curval
        cron.append(jobdict)
    if request.method == "GET":
        return render_template('esch.html', all=cron, id=id)
    if request.method == "POST":
        i = request.form.get('schid')
        m = request.form.get('minute')
        h = request.form.get('hour')
        dy = request.form.get('day')
        mh = request.form.get('month')
        dow = request.form.get('day_of_week')
        j = scheduler.modify_job(id,
                            trigger='cron', 
                            minute=f'{m}', 
                            hour=f'{h}',
                            day=f'{dy}',
                            month=f'{mh}',
                            day_of_week=f'{dow}')
        return redirect(url_for('showsch', mo=j))
    
@app.route("/cschstatus/<id>", methods=["POST"])
def cschstatus(id):
    r = request.form.get('start')
    p = request.form.get('stop')
    if r == "start":
        scheduler.resume_job(id=id)
        return redirect(url_for('showsch'))
    elif p == "stop":
        scheduler.pause_job(id=id)
        return redirect(url_for('showsch'))

@app.route("/addsch", methods=["GET","POST"])
def addsch():
    if request.method == "GET":
        all = db.all()
        return render_template('addsch.html', all=all)
    if request.method == "POST":
        name = request.form.get('schname')
        m = request.form.get('minute')
        h = request.form.get('hour')
        dy = request.form.get('day')
        mh = request.form.get('month')
        dow = request.form.get('day_of_week')
        tsk = request.form.get('task')
        add = scheduler.add_job(name,
                                func='execute:job',
                                args=[f'{tsk}'],
                                trigger='cron',
                                minute=f'{m}',
                                hour=f'{h}',
                                day=f'{dy}',
                                month=f'{mh}',
                                day_of_week=f'{dow}')
        return redirect(url_for('showsch',add=add))


@app.route("/delsch/<id>", methods=["POST"])
def delsch(id):
    dl = scheduler.delete_job(id)
    return redirect(url_for('showsch',dl=dl))

if __name__ == "__main__":
    app.run(debug=True)